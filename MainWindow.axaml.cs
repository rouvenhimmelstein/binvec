using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Avalonia.Media.Imaging;
using Avalonia.Svg.Skia;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Bitmap = Avalonia.Media.Imaging.Bitmap;
using Image = SixLabors.ImageSharp.Image;
using Rectangle = SixLabors.ImageSharp.Rectangle;

namespace BinVec
{
    public class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif

            imgInput = this.FindControl<Avalonia.Controls.Image>("imgInput");
            imgBinarized = this.FindControl<Avalonia.Controls.Image>("imgBinarized");
            imgVectorized = this.FindControl<Avalonia.Controls.Image>("imgVectorized");
            sldBinarize = this.FindControl<Avalonia.Controls.Slider>("sldBinarize");
            chkInvertImage = this.FindControl<Avalonia.Controls.CheckBox>("chkInvertImage");
            scaleCanvas = this.FindControl<Avalonia.Controls.Canvas>("scaleCanvas");
            scaleRectangle = this.FindControl<Avalonia.Controls.Shapes.Rectangle>("scaleRectangle");

            IObservable<double>? sliderValueObservable = sldBinarize.GetObservable(Slider.ValueProperty);
            sliderValueObservable.Subscribe(value => OnBinarize((int)value, chkInvertImage.IsChecked.GetValueOrDefault(false)));

            OnImageSelectedEvent += OnImageSelected;
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public delegate void ImageSelectedEvent(string imagePath);
        public event ImageSelectedEvent OnImageSelectedEvent;

        private Avalonia.Controls.Image imgInput;
        private Avalonia.Controls.Image imgBinarized;
        private Avalonia.Controls.Image imgVectorized;
        private Avalonia.Controls.Slider sldBinarize;
        private Avalonia.Controls.CheckBox chkInvertImage;
        private Avalonia.Controls.Canvas scaleCanvas;
        private Avalonia.Controls.Shapes.Rectangle scaleRectangle;

        private string imagePath;
        private Image<Rgba32> baseImage;
        private Image<Rgba32> binarizedImage;
        private bool isMouseDown;
        private Avalonia.Point scaleStartPoint = new Avalonia.Point(0, 0);
        private Avalonia.Point scaleEndPoint = new Avalonia.Point(0, 0);
        private Rgba32 WHITE = Rgba32.ParseHex("#FFFFFF");
        private Rgba32 BLACK = Rgba32.ParseHex("#000000");

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SelectImagePath();
        }

        private void OnImageSelected(string imagePath)
        {
            Avalonia.Threading.Dispatcher.UIThread.Post(() =>
            {
                this.imagePath = imagePath;
                baseImage = LoadImageFromFile(imagePath);
                imgInput.Source = ToBitmap(baseImage);
            });
        }

        private void SelectImagePath()
        {
            Task.Run(() =>
           {
               OpenFileDialog openFileDialog = new OpenFileDialog
               {
                   Title = "Bild ausw�hlen",
                   AllowMultiple = false
               };
               openFileDialog.Filters.Add(new FileDialogFilter() { Name = "Image", Extensions = { "jpg", "jpeg", "png", "bmp", "gif", "tga" } });
               var outPathStrings = openFileDialog.ShowAsync(this).GetAwaiter().GetResult();
               if (outPathStrings.Length > 0) OnImageSelectedEvent(outPathStrings[0]);
           });
        }

        private Image<Rgba32> LoadImageFromFile(string fileName)
        {
            Image<Rgba32> img = Image.Load<Rgba32>(fileName);
            img.Mutate(x => x.Resize(250, 250));
            return img;
        }

        private void btnBinarize_Click(object sender, RoutedEventArgs e)
        {
            OnBinarize((int)sldBinarize.Value, chkInvertImage.IsChecked.GetValueOrDefault(false));
        }

        private void chkInvertImage_Checked(object sender, RoutedEventArgs e)
        {
            OnBinarize((int)sldBinarize.Value, chkInvertImage.IsChecked.GetValueOrDefault(false));
        }

        private void OnBinarize(int treshold, bool invert)
        {
            if (baseImage == null)
                return;

            binarizedImage = Binarize(baseImage.Clone(), treshold, invert);
            imgBinarized.Source = ToBitmap(binarizedImage);
        }

        private Image<Rgba32> Binarize(Image<Rgba32> inputImage, int treshold, bool invert)
        {
            if (scaleStartPoint != new Avalonia.Point(0, 0) && scaleEndPoint != new Avalonia.Point(0, 0))
            {
                int width = (int)(scaleEndPoint.X - scaleStartPoint.X);
                int height = (int)(scaleEndPoint.Y - scaleStartPoint.Y);
                inputImage = CropImage(inputImage, new Rectangle((int)scaleStartPoint.X, (int)scaleStartPoint.Y, width, height));
            }

            Debug.Print("============");
            Debug.Print("start: " + scaleStartPoint.ToString());
            Debug.Print("end: " + scaleEndPoint.ToString());
            Debug.Print("============");

            for (int y = 0; y < inputImage.Height; y++)
            {
                Span<Rgba32> pixelRowSpan = inputImage.GetPixelRowSpan(y);
                for (int x = 0; x < inputImage.Width; x++)
                {
                    Rgba32 pixel = pixelRowSpan[x];
                    int greyScale = (pixel.R + pixel.G + pixel.B) / 3;
                    bool isWhite = greyScale > treshold;
                    isWhite = invert ? !isWhite : isWhite;

                    pixelRowSpan[x] = isWhite ? WHITE : BLACK;
                }
            }

            return inputImage;
        }

        private Image<Rgba32> CropImage(Image<Rgba32> source, Rectangle section)
        {
            if (section.Width <= 0)
                section.Width = 1;

            if (section.Height <= 0)
                section.Height = 1;

            try
            {
                source.Mutate(x => x.Crop(section));
            }
            catch (Exception e) { };

            return source;
        }

        public Image ToSharpImage(IBitmap bitmapImage)
        {
            using MemoryStream outStream = new MemoryStream();

            bitmapImage.Save(outStream);
            outStream.Position = 0;

            return Image.Load<Rgba32>(outStream);
        }

        public IBitmap ToBitmap(Image<Rgba32> sharpImage)
        {
            using var memory = new MemoryStream();

            sharpImage.SaveAsPng(memory);
            memory.Position = 0;

            return new Bitmap(memory);
        }

        private void btnVectorize_Click(object sender, RoutedEventArgs e)
        {
            if (binarizedImage == null)
                return;

            string imageName = Path.GetFileNameWithoutExtension(imagePath);
            string newImageName = imageName + ".binvec.bmp";
            string newImageNameSvg = imageName + ".binvec.svg";
            DirectoryInfo dir = Directory.GetParent(imagePath);
            string fullNewImageNameBmp = Path.Combine(dir.FullName, newImageName);
            string fullNewImageNameSvg = Path.Combine(dir.FullName, newImageNameSvg);

            binarizedImage.SaveAsBmp(fullNewImageNameBmp);

            VectorizeFile(fullNewImageNameBmp);

            var svgImage = new SvgImage();
            var svgSource = new SvgSource();
            var svgStream = File.OpenRead(fullNewImageNameSvg);
            svgSource.Load(svgStream);
            svgImage.Source = svgSource;
            imgVectorized.Source = svgImage;

            svgStream.Dispose();
            File.Delete(fullNewImageNameBmp);
            File.Delete(fullNewImageNameSvg);
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (imagePath == null)
                return;

            string imageName = Path.GetFileNameWithoutExtension(imagePath);
            string newImageName = imageName + ".bmp";
            DirectoryInfo dir = Directory.GetParent(imagePath);
            string fullNewImageNameBmp = Path.Combine(dir.FullName, newImageName);

            var image = Image.Load<Rgba32>(imagePath);
            var binarizedBitmap = Binarize(
                    image,
                    (int)sldBinarize.Value,
                    chkInvertImage.IsChecked.GetValueOrDefault(false)
                );
            binarizedBitmap.SaveAsBmp(fullNewImageNameBmp);

            VectorizeFile(fullNewImageNameBmp);

            File.Delete(fullNewImageNameBmp);
        }

        private void VectorizeFile(string filePath)
        {
            Process p = new Process();
            p.StartInfo = new ProcessStartInfo("potrace.exe", "--svg \"" + filePath + "\"");
            p.StartInfo.CreateNoWindow = true;
            p.Start();
            p.WaitForExit();
        }

        private void OnMouseDown(object sender, PointerPressedEventArgs e)
        {
            if (e.MouseButton == MouseButton.Right)
            {
                scaleEndPoint = new Avalonia.Point(0, 0);
                scaleStartPoint = new Avalonia.Point(0, 0);
                scaleRectangle.IsVisible = false;
                return;
            }

            isMouseDown = true;
            scaleStartPoint = e.GetPosition(scaleCanvas);
            scaleRectangle.IsVisible = true;
        }

        private void OnMouseUp(object sender, PointerReleasedEventArgs e)
        {
            isMouseDown = false;
            scaleEndPoint = e.GetPosition(scaleCanvas);
        }

        private void OnMouseMove(object sender, PointerEventArgs e)
        {
            if (!isMouseDown)
            {
                return;
            }

            //speichere drag end point
            scaleEndPoint = e.GetPosition(scaleCanvas);

            //berechne h�he und breite
            scaleRectangle.IsVisible = true;

            //platziere rectandgle
            if (scaleStartPoint.X < scaleEndPoint.X)
            {
                Canvas.SetLeft(scaleRectangle, scaleStartPoint.X);
                scaleRectangle.Width = scaleEndPoint.X - scaleStartPoint.X;
            }
            else
            {
                Canvas.SetLeft(scaleRectangle, scaleEndPoint.X);
                scaleRectangle.Width = scaleStartPoint.X - scaleEndPoint.X;
            }

            if (scaleStartPoint.Y < scaleEndPoint.Y)
            {
                Canvas.SetTop(scaleRectangle, scaleStartPoint.Y);
                scaleRectangle.Height = scaleEndPoint.Y - scaleStartPoint.Y;
            }
            else
            {
                Canvas.SetTop(scaleRectangle, scaleEndPoint.Y);
                scaleRectangle.Height = scaleStartPoint.Y - scaleEndPoint.Y;
            }
        }
    }
}
